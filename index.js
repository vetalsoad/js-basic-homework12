"use strict";

const buttons = document.querySelectorAll(".btn-wrapper .btn");
const btns = ["enter", "s", "e", "o", "n", "l", "z"];
console.log(buttons);

const doInBlue = function (event) {
  const letter = event.key.toLowerCase();
  const buttonIndex = btns.indexOf(letter);
  if (buttonIndex !== -1) {
    buttons.forEach((button) => (button.style.backgroundColor = "black"));
    buttons[buttonIndex].style.backgroundColor = "blue";
  }
};

document.addEventListener("keydown", doInBlue);
